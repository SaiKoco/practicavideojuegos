package com.simion.base;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.simion.resources.ResourcesManager;
import com.simion.util.Estado;

import static com.simion.util.Constantes.*;
import static com.simion.util.Constantes.NAVE_ABI;

/**
 * Created by Simion on 09/03/2017.
 */
public class Enemigo {

    protected TextureRegion frameActual;
    protected Animation animacion;
    public Vector2 posicion;
    public Rectangle rect;
    private float tiempo;
    public Estado estado;
    public int vidas;
    public int premio;
    public Vector2 velocidad;
    public enum Tipo{
        STR, NAVE;
    }
    public ObjetoMovil.Tipo tipo;

    public Enemigo(float x, float y, String nombreAnimacion, int puntosVida, int premio) {
        this.vidas = puntosVida;
        this.premio = premio;
        posicion = new Vector2(x, y);
        velocidad = new Vector2(0,0);

        animacion = new Animation(0.075f,
                ResourcesManager.obtenerAnimacion(nombreAnimacion));
        estado = Estado.PARADA;
        tiempo = 0;

        frameActual = (TextureRegion) animacion.getKeyFrame(0);
        rect = new Rectangle(posicion.x, posicion.y,
                frameActual.getRegionWidth(), frameActual.getRegionHeight());
    }

    public void update(float delta){
        tiempo += delta;

        frameActual = (TextureRegion) animacion.getKeyFrame(tiempo, true);

        posicion.add(velocidad);
        rect.setPosition(posicion);
    }

    public void render(Batch batch) {
        batch.draw(frameActual, posicion.x, posicion.y);
    }

    public void dispose() {
    }

}

package com.simion.base;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.simion.resources.ResourcesManager;
import com.simion.util.Estado;

import static com.simion.util.Constantes.SPEED_BULLET;

/**
 * Created by Simion on 09/03/2017.
 */
public abstract class Municion {

    public TextureRegion imagen;
    public Vector2 posicion;
    public Rectangle rect;
    public Estado estado;

    public Municion(TextureRegion imagen, float x, float y, Estado estdo){
        this.imagen = imagen;
        posicion = new Vector2(x, y);
        this.estado = estdo;
        rect = new Rectangle(x, y, this.imagen.getRegionWidth(), this.imagen.getRegionHeight());
    }

    public void mover(){
        System.out.println(estado);
        if (estado.equals(Estado.ABAJO)){
            posicion.y -= SPEED_BULLET;
        }
        else if (estado.equals(Estado.IZQUIERDA)){
            posicion.x -= SPEED_BULLET;
        }
        else if (estado.equals(Estado.ARRIBA)){
            posicion.y += SPEED_BULLET;
        }
        else if (estado.equals(Estado.DERECHA)){
            posicion.x += SPEED_BULLET;
        }
    }

    public void render(Batch batch) {
        batch.draw(imagen, posicion.x, posicion.y);
    }

    public void dispose() {
    }
}

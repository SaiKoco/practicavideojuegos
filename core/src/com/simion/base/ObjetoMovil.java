package com.simion.base;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.simion.resources.ResourcesManager;
import com.simion.util.Estado;

import static com.simion.util.Constantes.*;

/**
 * Created by Simion on 09/03/2017.
 */
public class ObjetoMovil {

    protected TextureRegion frameActual;
    protected Animation animacion;
    public Vector2 posicion;
    public Rectangle rect;
    private float tiempo;
    public Estado estado;
    public int vidas;
    public Vector2 velocidad;
    public enum Tipo{
        STR, NAVE;
    }
    public Tipo tipo;

    public ObjetoMovil(float x, float y, String nombreAnimacion) {

        posicion = new Vector2(x, y);
        velocidad = new Vector2(0,0);

        animacion = new Animation(0.25f,
                ResourcesManager.obtenerAnimacion(nombreAnimacion));

        estado = Estado.PARADA;
        tiempo = 0;

        frameActual = (TextureRegion) animacion.getKeyFrame(0);
        rect = new Rectangle(posicion.x, posicion.y,
                frameActual.getRegionWidth(), frameActual.getRegionHeight());
    }

    public void update(float delta){
        tiempo += delta;

        switch (estado) {
            case ABAJO:
                frameActual = ResourcesManager.obtenerFrame(NAVE_ABJ);
                break;
            case ABJDCH:
                frameActual = ResourcesManager.obtenerFrame(NAVE_ABD);
                break;
            case DERECHA:
                frameActual = ResourcesManager.obtenerFrame(NAVE_DCH);
                break;
            case ARRDCH:
                frameActual = ResourcesManager.obtenerFrame(NAVE_AD);
                break;
            case ARRIBA:
                frameActual = ResourcesManager.obtenerFrame(NAVE_ARR);
                break;
            case ARRIZQ:
                frameActual = ResourcesManager.obtenerFrame(NAVE_AI);
                break;
            case IZQUIERDA:
                frameActual = ResourcesManager.obtenerFrame(NAVE_IZQ);
                break;
            case ABJIZQ:
                frameActual = ResourcesManager.obtenerFrame(NAVE_ABI);
                break;
            default:
                break;
        }
        posicion.add(velocidad);
        rect.setPosition(posicion);
    }

    public void render(Batch batch) {
        batch.draw(frameActual, posicion.x, posicion.y);
    }

    public void dispose() {
    }
}

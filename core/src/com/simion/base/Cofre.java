package com.simion.base;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.simion.resources.ResourcesManager;

/**
 * Created by Simion on 09/03/2017.
 */
public class Cofre {

    protected TextureRegion imagen;
    protected Animation animacion;
    public Rectangle rect;
    public int puntuacion;
    private float tiempo;

    public Cofre(float x, float y, int puntuacion) {


        animacion = new Animation(0.075f,
                ResourcesManager.obtenerAnimacion("cofre"));
        imagen =  (TextureRegion) animacion.getKeyFrame(0);
        rect = new Rectangle(x, y, imagen.getRegionWidth(), imagen.getRegionHeight());
        this.puntuacion = puntuacion;
        tiempo = 0;
    }


    public void update(float delta){
        tiempo += delta;
        imagen = (TextureRegion) animacion.getKeyFrame(tiempo, true);
    }
    public void render(Batch batch) {
        batch.draw(imagen, rect.x, rect.y);
    }
}

package com.simion.base;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.simion.resources.ResourcesManager;

import static com.simion.util.Constantes.*;
import static com.simion.util.Constantes.NAVE_ABI;

/**
 * Created by Simion on 09/03/2017.
 */
public class Nave extends ObjetoMovil{

    public int puntos;
    private int tiempo;

    public Nave(float x, float y) {
        super(x, y, NAVE_DCH);
        puntos = 0;
        tiempo = 0;
        tipo = Tipo.NAVE;
        vidas = 3;
    }


}

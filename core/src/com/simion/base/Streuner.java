package com.simion.base;

/**
 * Created by Simion on 09/03/2017.
 */
public class Streuner extends ObjetoMovil{

    public int premio;

    public Streuner(float x, float y, int puntosVida, int premio) {
        super(x, y, "streuner_derecha");
        tipo = Tipo.STR;
        this.premio = premio;
        vidas = puntosVida;
    }
}

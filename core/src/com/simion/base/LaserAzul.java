package com.simion.base;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.simion.util.Estado;

/**
 * Created by Simion on 09/03/2017.
 */
public class LaserAzul extends Municion {

    public LaserAzul(float x, float y, TextureRegion imagen, Estado estado) {
        super(imagen, x, y, estado);
        this.estado = estado;
    }
}


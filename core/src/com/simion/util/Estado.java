package com.simion.util;

/**
 * Created by Simion on 09/03/2017.
 */
public enum Estado {
    ARRIBA, ABAJO, DERECHA, IZQUIERDA,
    ARRIZQ, ARRDCH, ABJDCH, ABJIZQ, PARADA;
}

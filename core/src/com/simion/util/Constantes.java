package com.simion.util;

import org.omg.CORBA.PUBLIC_MEMBER;
import sun.awt.AWTAccessor;

/**
 * Created by Simion on 09/03/2017.
 */
public class Constantes {

    public static final String PK_ATLAS = "atlas.pack";
    public static final String FONDO_MAIN = "fondo3.jpg";

    public static final String NAVE_DCH = "nave_derecha";
    public static final String NAVE_IZQ = "nave_izq";
    public static final String NAVE_ARR = "nave_arriba";
    public static final String NAVE_ABJ = "nave_abajo";
    public static final String NAVE_AI = "nave_ai";
    public static final String NAVE_AD = "nave_ad";
    public static final String NAVE_ABD = "nave_abd";
    public static final String NAVE_ABI = "nave_abi";

    public static final String CP_COLISION = "Colision";
    public static final String CP_COFRE = "Cofre";
    public static final String CP_KRISTALLIN = "Kristallin";
    public static final String CP_LORDAKIUM = "Lordakium";
    public static final String CP_STREUNER = "Streuner";
    public static final String CP_PORTAL = "Portal";

    public static final String PUNTOS = "puntos";
    public static final String PUNTOS_VIDA = "puntosVida";
    public static final String PREMIO = "premio";

    public static final String _1024x768 = "1024x768";
    public static final String _800x600 = "800x600";
    public static final String _640x480 = "640x480";

    public static final int ANCHURA = 800;
    public static final int ALTURA = 600;

    public static final int ANCHURA_CAMARA = 40;
    public static final int ALTURA_CAMARA = 35;
    public static final int SIZE_TILED = 30;
    public static final int WEIGHT_TILED = 244;
    public static final int HEIGHT_TILED = 134;
    public static final int WEIGHT_TILED2 = 208;
    public static final int HEIGHT_TILED2 = 127;

    public static final String FILE_MUSIC = "audoDarkOrbit.wav";

    public static final int SPEED_BULLET = 20;
}

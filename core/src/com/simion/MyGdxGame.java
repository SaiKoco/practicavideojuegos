package com.simion;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.simion.resources.ResourcesManager;
import com.simion.screens.MainScreen;

public class MyGdxGame extends Game {
	public SpriteBatch batch;
	public Texture fondo;
	public BitmapFont fuente;
	
	@Override
	public void create () {
		ResourcesManager.cargarRecursos();
		batch = new SpriteBatch();
		fondo = new Texture("fondoMain.png");
		fuente = new BitmapFont(Gdx.files.internal("Halo.fnt"));
		setScreen(new MainScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		fondo.dispose();
	}
}

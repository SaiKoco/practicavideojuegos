package com.simion.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import static com.simion.util.Constantes.FILE_MUSIC;
import static com.simion.util.Constantes.FONDO_MAIN;
import static com.simion.util.Constantes.PK_ATLAS;

/**
 * Created by Simion on 09/03/2017.
 */
public class ResourcesManager {

    private static AssetManager assetManager = new AssetManager();

    /**
     * Carga todos los assets del juego
     */
    public static void cargarRecursos(){

        assetManager.load(PK_ATLAS, TextureAtlas.class);
        assetManager.load(FONDO_MAIN, Texture.class);
//        assetManager.load("explosion.wav", Sound.class);
        assetManager.load(FILE_MUSIC, Music.class);
        assetManager.finishLoading();
    }

    /**
     * Obtiene el Frame de la animacion.
     * @param nombreAnimacion
     * @return
     */
    public static Array<TextureAtlas.AtlasRegion> obtenerAnimacion(String nombreAnimacion){
        return assetManager.get(PK_ATLAS, TextureAtlas.class).findRegions(nombreAnimacion);
    }

    /**
     * Obtiene el primer frame de la animacion.
     * @param nombreFrame
     * @return
     */
    public static TextureRegion obtenerFrame(String nombreFrame){
        return assetManager.get(PK_ATLAS, TextureAtlas.class).findRegion(nombreFrame);
    }

    public static Music obtenerMusica(String nombreMusica){
        Music music = assetManager.get(nombreMusica,Music.class);

        return music;
    }
}

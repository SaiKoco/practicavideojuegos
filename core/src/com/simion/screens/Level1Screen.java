package com.simion.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.simion.MyGdxGame;
import com.simion.base.*;
import com.simion.resources.ResourcesManager;
import com.simion.util.Estado;

import static com.simion.util.Constantes.*;

/**
 * Created by Simion on 09/03/2017.
 */
public class Level1Screen implements Screen {
    private final MyGdxGame juego;
    BitmapFont fuente;
    TiledMap mapa;
    OrthogonalTiledMapRenderer mapRenderer;
    OrthographicCamera camara;
    Nave nave;
    Array<Cofre> cofres;
    private Batch batch;
    boolean tecla;
    boolean esquinas;
    Array<Streuner> streuners;
    Array<Lordakium> lordakiums;
    private Array<LaserAzul> lasersAzules;

    public Level1Screen(MyGdxGame juego) {
        this.juego = juego;
        batch = juego.batch;
        fuente = juego.fuente;
    }

    @Override
    public void show() {

        nave = new Nave(0, 50);
        cofres = new Array<Cofre>();
        streuners = new Array<Streuner>();
        lordakiums = new Array<Lordakium>();
        lasersAzules = new Array<LaserAzul>();

        camara = new OrthographicCamera();
        camara.setToOrtho(false,
                ANCHURA_CAMARA * SIZE_TILED,
                ALTURA_CAMARA * SIZE_TILED);
        camara.update();

        mapa = new TmxMapLoader().load("levels/MapaNivel1.tmx");
        mapRenderer = new OrthogonalTiledMapRenderer(mapa);
        batch = mapRenderer.getBatch();

        mapRenderer.setView(camara);

        cargarMapa();

    }

    private void cargarMapa() {
        MapLayer capaObjetos = mapa.getLayers().get(CP_COFRE);
        MapObjects mapObjects = capaObjetos.getObjects();
        for (MapObject mapObject : mapObjects) {
            int puntuacion = Integer.parseInt(String.valueOf(mapObject.getProperties().get(PUNTOS)));
            TiledMapTileMapObject tileMapObject =
                    (TiledMapTileMapObject) mapObject;
            Cofre cofre = new Cofre(
                    tileMapObject.getX(), tileMapObject.getY(), puntuacion);
            cofres.add(cofre);

        }

        capaObjetos = mapa.getLayers().get(CP_STREUNER);
        mapObjects = capaObjetos.getObjects();
        for (MapObject mapObject: mapObjects){
            int puntosVida = Integer.parseInt(String.valueOf(mapObject.getProperties().get(PUNTOS_VIDA)));
            int premio = Integer.parseInt(String.valueOf(mapObject.getProperties().get(PREMIO)));
            TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObject;
            Streuner str = new Streuner(
                    tileMapObject.getX(), tileMapObject.getY(), premio, puntosVida);
            streuners.add(str);
        }

        capaObjetos = mapa.getLayers().get(CP_LORDAKIUM);
        mapObjects = capaObjetos.getObjects();
        for (MapObject mapObject: mapObjects){
            int puntosVida = Integer.parseInt(String.valueOf(mapObject.getProperties().get(PUNTOS_VIDA)));
            int premio = Integer.parseInt(String.valueOf(mapObject.getProperties().get(PREMIO)));
            TiledMapTileMapObject tileMapObject = (TiledMapTileMapObject) mapObject;
            Lordakium lord = new Lordakium(
                    tileMapObject.getX(), tileMapObject.getY(), "lordakium",
                    premio, puntosVida);
            lordakiums.add(lord);
        }


    }

    @Override
    public void render(float delta) {

        if (nave.vidas == 0){
            juego.setScreen(new GameOverScreen(juego, nave.puntos));
        }

        // Limpia la pantalla
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mapRenderer.render(new int[]{0, 1});
        batch.begin();
        nave.render(batch);
        nave.update(delta);
        for (Cofre cofre : cofres){
            cofre.render(batch);
            cofre.update(delta);
        }
        for (Streuner str: streuners){
            str.render(batch);
            str.update(delta);
        }
        for (Lordakium lord : lordakiums){
            lord.render(batch);
            lord.update(delta);
        }
        for (LaserAzul laserAzul: lasersAzules){
            laserAzul.render(batch);
        }
        fuente.getData().setScale(2);
        fuente.draw(batch, "Puntos: " + nave.puntos,
                camara.position.x - ANCHURA_CAMARA/2*SIZE_TILED + 25,
                camara.position.y + ALTURA_CAMARA/2*SIZE_TILED);
        fuente.draw(batch, "Vidas: " + nave.vidas,
                camara.position.x + ANCHURA_CAMARA/2*SIZE_TILED - 300,
                camara.position.y  + ALTURA_CAMARA/2*SIZE_TILED );
        batch.end();


        moverLasers();
        comprobarTeclado(delta);
        fijarCamara();
        comprobarColisiones();

    }

    private void moverLasers() {
        for (LaserAzul laserAzul: lasersAzules){
            laserAzul.mover();
        }
    }

    private void comprobarColisiones() {

        /*
        Comprueba la colisión con el suelo
         */
        MapLayer capaColision = mapa.getLayers().get(CP_COLISION);
        MapObjects mapObjects = capaColision.getObjects();
        for (MapObject mapObject : mapObjects) {
            Rectangle rect = ((RectangleMapObject) mapObject).getRectangle();
            if (rect.overlaps(nave.rect)) {
                if (mapObject.getName().equals("izquierda")){
                    nave.posicion.x = rect.x + rect.getWidth();
                }
                else if (mapObject.getName().equals("derecha")){
                    nave.posicion.x = rect.x - nave.rect.getWidth();
                }
                else if (mapObject.getName().equals("arriba")){
                    nave.posicion.y = rect.y - nave.rect.getHeight();
                }
                else if (mapObject.getName().equals("abajo")){
                    nave.posicion.y = rect.y + rect.getHeight();
                }
            }

        }
        MapLayer capaPortal = mapa.getLayers().get(CP_PORTAL);
        mapObjects = capaPortal.getObjects();
        for (MapObject mapObject : mapObjects) {
            Rectangle rect = ((RectangleMapObject) mapObject).getRectangle();
            if (rect.overlaps(nave.rect)) {
                juego.setScreen(new Level2Screen(juego, nave.puntos, nave.vidas));
                break;
            }
        }
        /**
        Comprueba la colisión con los diamantes
         */
        for (Cofre cofre : cofres){
            if (cofre.rect.overlaps(nave.rect)){
                nave.puntos += cofre.puntuacion;
                cofres.removeValue(cofre, true);
            }
        }
        for (Streuner str: streuners){
            for (LaserAzul laserAzul: lasersAzules){
                if (str.rect.overlaps(laserAzul.rect)){
                    if (str.vidas == 0){
                        nave.puntos += str.premio;
                        streuners.removeValue(str, true);
                        lasersAzules.removeValue(laserAzul, true);
                    }
                    else {
                        str.vidas --;
                        lasersAzules.removeValue(laserAzul, true);
                    }
                }
            }
            if (str.rect.overlaps(nave.rect)){
                nave.vidas --;
                streuners.removeValue(str, true);
            }
        }
        for (Lordakium lord : lordakiums){
            for (LaserAzul laserAzul: lasersAzules){
                if (lord.vidas == 0){
                    nave.puntos += lord.premio;
                    lordakiums.removeValue(lord, true);
                    lasersAzules.removeValue(laserAzul, true);
                }
                else {
                    lord.vidas --;
                    lasersAzules.removeValue(laserAzul, true);
                }
            }
            if (lord.rect.overlaps(nave.rect)){
                nave.vidas --;
                lordakiums.removeValue(lord, true);
            }
        }

    }

    private void fijarCamara() {

        esquinas = false;
        float x = nave.posicion.x;
        float y = nave.posicion.y;
        if (nave.posicion.x < ANCHURA_CAMARA/2 * SIZE_TILED ){
            x = ANCHURA_CAMARA/2*SIZE_TILED;
            esquinas = true;
        }
        else if (nave.posicion.x > (WEIGHT_TILED-ANCHURA_CAMARA/2)*SIZE_TILED){
            x = (WEIGHT_TILED-ANCHURA_CAMARA/2)*SIZE_TILED;
            esquinas = true;
        }
        if (nave.posicion.y < ALTURA_CAMARA/2f * SIZE_TILED){
            y =  ALTURA_CAMARA/2f * SIZE_TILED;
            esquinas = true;
        }
        else if (nave.posicion.y > (HEIGHT_TILED-ALTURA_CAMARA/2f)*SIZE_TILED){
            y = (HEIGHT_TILED-ALTURA_CAMARA/2f)*SIZE_TILED;
            esquinas = true;
        }

        camara.position.set(x, y, 0);

        camara.update();
        mapRenderer.setView(camara);

    }

    private void comprobarTeclado(float delta) {

        int masDelta = 1000;
        tecla = false;
        if (Gdx.input.isKeyPressed(Input.Keys.UP)){
            nave.velocidad.y = masDelta * delta;
            nave.estado = Estado.ARRIBA;
            tecla = true;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)){
            nave.velocidad.y = -masDelta * delta;
            nave.estado = Estado.ABAJO;
            tecla = true;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            nave.velocidad.x = masDelta * delta;
            nave.estado = Estado.DERECHA;
            tecla = true;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            nave.velocidad.x = -masDelta * delta;
            nave.estado = Estado.IZQUIERDA;
            tecla = true;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)&&Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            nave.velocidad.x = -masDelta * delta;
            nave.velocidad.y = masDelta * delta;
            nave.estado = Estado.ARRIZQ;
            tecla = true;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.UP)&&Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            nave.velocidad.x = masDelta * delta;
            nave.velocidad.y = masDelta * delta;
            nave.estado = Estado.ARRDCH;
            tecla = true;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)&&Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            nave.velocidad.x = masDelta * delta;
            nave.velocidad.y = -masDelta * delta;
            nave.estado = Estado.ABJDCH;
            tecla = true;
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)&&Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            nave.velocidad.x = -masDelta * delta;
            nave.velocidad.y = -masDelta * delta;
            nave.estado = Estado.ABJIZQ;
            tecla = true;
        }

        if (!tecla){
            nave.velocidad.y = 0;
            nave.velocidad.x = 0;
        }



        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            TextureRegion foto;
            if (nave.estado == Estado.ARRIBA){
                foto = ResourcesManager.obtenerFrame("laser_up");
                LaserAzul laser = new LaserAzul(
                        nave.posicion.x + nave.rect.getWidth()/2,
                        nave.posicion.y + nave.rect.getHeight(),
                        foto,
                        nave.estado);
                lasersAzules.add(laser);
            }
            if (nave.estado == Estado.DERECHA){
                foto = ResourcesManager.obtenerFrame("laser_right");
                LaserAzul laser = new LaserAzul(
                        nave.posicion.x + nave.rect.getWidth(),
                        nave.posicion.y + nave.rect.getHeight()/2,
                        foto,
                        nave.estado);
                lasersAzules.add(laser);
            }
             if (nave.estado == Estado.ABAJO){
                foto = ResourcesManager.obtenerFrame("laser_down");
                LaserAzul laser = new LaserAzul(
                        nave.posicion.x + nave.rect.getWidth()/2,
                        nave.posicion.y,
                        foto,
                        nave.estado);
                lasersAzules.add(laser);
            }
              if (nave.estado == Estado.IZQUIERDA){
                foto = ResourcesManager.obtenerFrame("laser_left");
                LaserAzul laser = new LaserAzul(
                        nave.posicion.x - foto.getRegionWidth() ,
                        nave.posicion.y + nave.rect.getHeight()/2,
                        foto,
                        nave.estado);
                lasersAzules.add(laser);
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
            juego.setScreen(new MainScreen(juego));
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        fuente.dispose();
    }
}

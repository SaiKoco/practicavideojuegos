package com.simion.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.simion.MyGdxGame;
import com.simion.resources.ResourcesManager;

import static com.simion.util.Constantes.FILE_MUSIC;

/**
 * Created by Simion on 09/03/2017.
 */
public class MainScreen implements Screen {

    private final MyGdxGame juego;
    Stage escena;

    public MainScreen(MyGdxGame juego) {
        this.juego = juego;
    }

    @Override
    public void show() {

        Music music = ResourcesManager.obtenerMusica(FILE_MUSIC);
        music.play();

        if(!VisUI.isLoaded()){
            VisUI.load();
        }

        // Crea la escena
        escena = new Stage();
        // Crea una tabla que servirá de layout para los componentes de la UI
        VisTable table = new VisTable();
        table.setFillParent(true);
        // Añade la tabla a la escena
        escena.addActor(table);

        // Crea un botón, le añade un listener y lo añade a la tabla
        VisTextButton btJugar = new VisTextButton("Jugar");

        btJugar.setSize(200, 60);
        btJugar.setPosition(table.getWidth()/2 - btJugar.getWidth()/2, 40);
        btJugar.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                // Qué hacer al pulsar el botón
                juego.setScreen(new Level1Screen(juego));
                dispose();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                // Qué hacer al soltar el botón
            }
        });

        VisTextButton btConfig = new VisTextButton("Configuración");
        btConfig.setSize(200, 60);
        btConfig.setPosition(table.getWidth()/2 - btConfig.getWidth()/2, 0);
        btConfig.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                // Qué hacer al pulsar el botón
                juego.setScreen(new ConfigScreen(juego));
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                // Qué hacer al soltar el botón
            }
        });

        VisTextButton btSalir = new VisTextButton("Salir");
        btSalir.setSize(200, 60);
        btSalir.setPosition(table.getWidth()/2 - btSalir.getWidth()/2, 0);
        btSalir.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                // Qué hacer al pulsar el botón
                juego.dispose();
                System.exit(0);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                // Qué hacer al soltar el botón
            }
        });
        table.row();
        table.add(btJugar).center().pad(5).width(200).height(50);
        table.row();
        table.add(btConfig).center().pad(5).width(200).height(50);
        table.row();
        table.add(btSalir).center().pad(5).width(200).height(50);

        // Activa el input de usuario para la escena
        Gdx.input.setInputProcessor(escena);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        juego.batch.begin();
        juego.batch.draw(juego.fondo, 0, 0);
        juego.batch.end();
        escena.act(delta);
        escena.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

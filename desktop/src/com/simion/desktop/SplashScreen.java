package com.simion.desktop;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Simion on 09/03/2017.
 */
public class SplashScreen {
    private JPanel panel1;
    private JLabel lbImagen;
    public SplashScreen(String urlFoto) {
        JFrame frame = new JFrame("SplashScreen");
        lbImagen.setIcon(new ImageIcon(urlFoto));
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setUndecorated(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        try{
            Thread.sleep(1000);
            //fixme poner mas tiempo de retardo.
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        frame.dispose();
    }
}

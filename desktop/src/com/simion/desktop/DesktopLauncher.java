package com.simion.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.simion.MyGdxGame;
import com.simion.util.Constantes;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "VideoJuego de Simion Bordean";
		config.width = Constantes.ANCHURA;
		config.height = Constantes.ALTURA;
//		SplashScreen sp = new SplashScreen("splash.png"); //FIXME descomentar para ver splashscreen
		new LwjglApplication(new MyGdxGame(), config);
	}
}

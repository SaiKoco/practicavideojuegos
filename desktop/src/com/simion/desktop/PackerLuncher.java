package com.simion.desktop;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

/**
 * Created by Simion on 09/03/2017.
 */
public class PackerLuncher {

    public static void main(String[] args){
        TexturePacker2.Settings settings = new TexturePacker2.Settings();
        settings.maxHeight = 4096;
        settings.maxWidth = 4096;
        settings.filterMag = Texture.TextureFilter.Linear;
        settings.filterMin = Texture.TextureFilter.Linear;

        TexturePacker2.process(settings, "core/assets/texturas", "core/assets", "atlas.pack");
    }
}
